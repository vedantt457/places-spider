import scrapy
import pycountry
import datetime
import json
import re

from locations.categories import Code
from locations.items import GeojsonPointItem
from typing import List

def parse_hours(hours):
    parsed_hours = []

    osm_days = {
        0: 'Mo',
        1: 'Tu',
        2: 'We',
        3: 'Th',
        4: 'Fr',
        5: 'Sa',
        6: 'Su'
    }

    for hour in hours:
        day = hour['dayOfWeek']
        opening_time = datetime.datetime.strptime(hour['openingTime'], '%H:%M:%S').time()
        closing_time = datetime.datetime.strptime(hour['closingTime'], '%H:%M:%S').time()

        opening_time_str = opening_time.strftime('%H:%M')
        closing_time_str = closing_time.strftime('%H:%M')

        parsed_hour = f"{osm_days[day]} {opening_time_str}-{closing_time_str}"
        parsed_hours.append(parsed_hour)
 
    parsed_hours.append("PH off")

    return "; ".join(parsed_hours)

def parse_phone(phone):
    return re.sub(r'[^0-9]', '', phone)


class VansMexSpider(scrapy.Spider):
    name = 'vans_mex_dac'
    brand = 'VANS_MEX'
    spider_chain_id = '8290'
    spider_type = 'chain'
    spider_categories: List[str] = [Code.CLOTHING_AND_ACCESSORIES.value]
    spider_countries: List[str] = [pycountry.countries.lookup('MEX').alpha_3]
    allowed_domains = ['vans.mx']
    start_urls = ['https://www.vans.mx/_v/private/graphql/v1?workspace=master&maxAge=long&appsEtag=remove&domain=store&locale=es-MX']

    def start_requests(self):
        headers = {
            'Content-Type': 'application/json',
            'Referer': 'https://www.vans.mx/stores',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'
        }
        
        payload = {
            'operationName': 'getStores',
            'variables': {},
            'extensions': {
                'persistedQuery': {
                    'version': 1,
                    'sha256Hash': '84c1d7c12d4b507dff72e8579bde396ec66c33f78d94188fcf756bfa14cc8518',
                    'sender': 'vtex.store-locator@0.x',
                    'provider': 'vtex.store-locator@0.x'
                }
            }
        }

        processed_stores = set()  

        for url in self.start_urls:
            yield scrapy.Request(url=url, headers=headers, method='POST', body=json.dumps(payload), callback=self.parse, meta={'processed_stores': processed_stores})

    def parse(self, response):
        if response.status == 200:
            responseData = response.json()
            
            items = responseData['data']['getStores']['items']
            processed_stores = response.meta['processed_stores']

            for item in items:
                Storeid = item['id']

                if id in processed_stores:
                    continue  

                name = item['name']
                instructions = item['instructions']

                address = item['address']
                addr_full = (
                    str(address['street']) + ', ' +
                    str(address['number']) + ', ' +
                    str(address['state']) + ', ' +
                    str(address['postalCode']) + ', ' +
                    str(address['city'])
                )
                housenumber = address['number']
                postalCode = address['postalCode']
                city = address['city']
                state = address['state']
                street = address['street']

                location = address['location']
                latitude = location['latitude']
                longitude = location['longitude']

                businessHours = item['businessHours']

                opening_hours = parse_hours(businessHours)

                item = GeojsonPointItem()
                item['ref'] = Storeid
                item['brand'] = self.brand
                item['chain_name'] = self.brand
                item['chain_id'] = self.spider_chain_id
                item['name'] = name
                item['addr_full'] = addr_full
                item['housenumber'] = housenumber
                item['street'] = street
                item['city'] = city
                item['state'] = state
                item['postcode'] = postalCode
                item['country'] = 'México'               
                phones = re.findall(r'[\+\d\s-]+', instructions)
                item['phone'] = [parse_phone(phone) for phone in phones]
                item['email'] ='contactovans@ecomsur.com'
                item['website'] = 'https://www.vans.mx'
                item['opening_hours'] = opening_hours
                item['lat'] = float(latitude)
                item['lon'] = float(longitude)

                processed_stores.add(Storeid)  

                yield item
